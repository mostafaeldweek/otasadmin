import { asyncRoutes } from "@/router/asyncRoutes";

export default {
  state: () => ({
    routes: []
  }),
  mutations: {
    SET_ROUTES(state, payload) {
      state.routes = payload;
    }
  },
  actions: {
    setAllowedRoutes({ commit }) {
      commit("SET_ROUTES", asyncRoutes);
    }
  },
  getters: {
    getRoutes(state) {
      return state.routes;
    }
  }
};
