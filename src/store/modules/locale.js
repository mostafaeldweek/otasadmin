import cookie from "js-cookie";
export default {
  state: () => ({
    locale: cookie.get("language") || "en"
  }),
  mutations: {
    SET_LOCALE(state, locale) {
      state.locale = locale;
    }
  },
  actions: {
    setLocale({ commit }, payload) {
      commit("SET_LOCALE", payload);
    }
  },
  getters: {
    getLocale(state) {
      return state.locale;
    }
  }
};
