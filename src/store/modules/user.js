import { getToken, setToken, removeToken } from "@/utils/auth";
import axios from "@/plugins/axios";
export default {
  state: () => ({
    token: getToken(),
    user: "",
  }),
  mutations: {
    SET_USER(state, user) {
      state.user = user;
    },
    SET_TOKEN(state, token) {
    state.token = token;
      console.log(token);
      setToken(token);
    },
  },
  actions: {
    Login({ commit, dispatch }, payload) {
      return new Promise((resolve, reject) => {
        axios.post(`login`, payload)
          .then((response) => {
            const { data } = response;
        
            commit("SET_TOKEN", data.token);
            dispatch("GetUserInfo");
            resolve();
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    GetUserInfo({ commit }) {
      return new Promise((resolve, reject) => {
        axios.get(`show-account`)
          .then((res) => {
            const { user } = res.data;
            commit("SET_USER", user);
            resolve(user);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    Logout({ commit }) {
      return new Promise((resolve, reject) => {
        commit("SET_TOKEN", "");
        removeToken();
        return resolve();
      });
    },
    setUserInfo({ commit }, user) {
      commit("SET_USER", user);
    },
  },
  getters: {
    getUser(state) {
      return state.user;
    },
  },
};

