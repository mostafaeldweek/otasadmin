import Vue from "vue";
import "@/assets/fonts/main.scss";
import "@/assets/scss/main.scss";
import "@/shared";
import "@/mixins";
import "@/plugins/vee-validate";
import App from "@/App.vue";
import router from "@/router";
import store from "@/store";
import vuetify from "@/plugins/vuetify";
import i18n from "@/plugins/i18n";
import axios from "@/plugins/axios";

Vue.prototype.axios = axios;
Vue.prototype.$eventBus = new Vue();
Vue.config.productionTip = false;

new Vue({
  store,
  router,
  vuetify,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
