import Vue from "vue";
import Vuetify from "vuetify/lib/framework";
import ChangeDirection from "@/utils/ChangeDirection";
Vue.use(Vuetify);
export default new Vuetify({
  rtl: ChangeDirection(),
  icons: {
    iconfont: "mdi"
  },
  theme: {
    themes: {
      light: {
        primary: '#0f6ffd',
        secondary: '#99cb3b',
        erorr: '#E10000',
        info: '#2196f3',
        warning: '#fb8c00',
        success: "#5ab805",
      },

    }
  },
});
