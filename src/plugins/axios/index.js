import axios from "axios";
import { baseURL } from "@/plugins/axios/baseUrl";
import { requestInterceptor, requestErrorInterceptor } from "./config/request";
import responseSuccessInterceptor from "./config/success";
import responseErrorInterceptor from "./config/error";
const api = axios.create({
  timeout: 60000,
  baseURL: baseURL,
});
api.interceptors.request.use(requestInterceptor, requestErrorInterceptor);
api.interceptors.response.use(responseSuccessInterceptor, responseErrorInterceptor);

export default api;
