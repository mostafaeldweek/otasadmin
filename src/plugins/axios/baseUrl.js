const base =()=> {
  if (process.env.NODE_ENV === "production") {
    return process.env.VUE_APP_ADMIN_PRODUCTION
  } else if (process.env.NODE_ENV === "testing") {
    return process.env.VUE_APP_ADMIN_TEST;
  }
  else {
    return process.env.VUE_APP_ADMIN_DEV;
  }
};
export const baseURL = base();

