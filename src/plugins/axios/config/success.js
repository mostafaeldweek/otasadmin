import store from "@/store";
const responseSuccessInterceptor = (response) => {
  store.dispatch("ClearServerErrors");
  const { method } = response.config;
  const successMethods = ["post", "put", "delete"];
  if (successMethods.includes(method) && response.data.message) {
    store.dispatch("ShowNotification", {
      text: `${response.data.message}`,
      color: "success",
    });
  }
  return response;
};

export default responseSuccessInterceptor;
