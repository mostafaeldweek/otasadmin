import store from "@/store";
const responseErrorInterceptor = (error) => {
  const { status, data } = error.response;
  if ([403, 503, 409, 422].includes(status)) {
    store.dispatch("ShowNotification", {
      text: data.message,
      color: "red",
    });
  }
  const { errors } = data;
  if (errors) {
    store.dispatch("SetServerErrors", errors);
  }
  return Promise.reject(error);
};

export default responseErrorInterceptor;
