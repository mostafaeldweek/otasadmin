import store from "@/store";
import { getToken } from "@/utils/auth";
const requestInterceptor = async (config) => {
  if (getToken()) {
    config.headers["Authorization"] = `Bearer ${getToken()}`;
  }
  const locale = store.getters["getLocale"];
  if (locale) {
    config.headers["X-locale"] = locale;
  }
  return config;
};
const requestErrorInterceptor = (error) => Promise.reject(error);
export { requestInterceptor, requestErrorInterceptor };
