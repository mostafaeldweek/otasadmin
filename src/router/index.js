import Vue from "vue";
import Router from "vue-router";
import Layout from "@/layout";
import cookie from "js-cookie";
import { asyncRoutes } from "./asyncRoutes";
Vue.use(Router);

export const baseRoutes = [
  {
    path: "/",
    component: Layout,
    meta: {
      title: "home"
    },
    children: [
      ...asyncRoutes
    ]
  },
  {
    path: "/login",
    component: () => import("@/views/login/index"),
    meta: {
      title: "login"
    },
    hidden: true
  },
  {
    path: "*",
    component: () => import("@/views/errorPage/404"),
    name: "Page404",
    meta: {
      title: "Page404"
    },
    hidden: true
  }
];

const router = new Router({
  mode: "history",
  routes: baseRoutes
});

router.beforeEach((to, from, next) => {

  const publicPaths = ["/login"];
  const isAuth = cookie.get("token");
  const isPublicPath = publicPaths.includes(to.path);

  if (isAuth && isPublicPath) {
    next("/home");

  } else if (!isAuth && !isPublicPath) {
    next("/login");

  }

  next();
});


export default router;
