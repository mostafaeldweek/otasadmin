export const topicsRoutes = {
  path: "/topics",
  to: "/topics",
  meta: {
    title: "topics",
    icon: "mdi-table-of-contents",
  },
  showInMenu: true,
  order : 2,
  component: () => import("@/views/topics"),
  children: [
    {
      path: "/",
      component: () => import("@/views/topics/record"),
      to: "/topics/record",
    },
    {
      path: "add",
      component: () => import("@/views/topics/actions"),
    },
    {
      path: ":id/edit",
      component: () => import("@/views/topics/actions"),
    }
  ]
};
