export const homeRoutes = {
  path: "/home",
  to: "/home",
  meta: {
    title: "home",
    icon: "mdi-table-of-contents",
  },
  showInMenu: true,
  order : 1,
  component: () => import("@/views/home"),
};
