import Vue from "vue";
import { mapGetters } from "vuex";
export const globalMixin = {
  computed: {
    ...mapGetters({
      pagination: "getPagination",
      user: "getUser",
      getQuery: "getQuery",
      getLocale: "getLocale"
    }),
  },
  methods: {
    convertDate(date) {
      return this.$moment(date).locale(this.$i18n.locale).format("LL");
    }
  },
};

Vue.mixin(globalMixin);
