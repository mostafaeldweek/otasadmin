import vue from "vue";
import FormGroup from "@/components/FormGroup";
import ShimmerLoader from "@/components/ShimmerLoader";
import CustomTable from "@/components/CustomTable";
import Pagination from "@/components/Pagination";
import Button from "@/components/Button";
import LocaleSelector from "@/components/LocaleSelector";
import ToggleService from "@/components/ToggleService";
import GenericDialog from "@/components/GenericDialog";
// components
vue.component("FormGroup", FormGroup);
vue.component("ShimmerLoader", ShimmerLoader);
vue.component("CustomTable", CustomTable);
vue.component("Pagination", Pagination);
vue.component("Button", Button);
vue.component("LocaleSelector", LocaleSelector);
vue.component("ToggleService", ToggleService);
vue.component("GenericDialog", GenericDialog);